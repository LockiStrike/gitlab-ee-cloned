module Timelogs
  class WriteIssueTimelogsToCsvService
    TIMELOG_FIELDS = { 
      'Time Spent'    => -> (timelog) { timelog.time_spent },
      'Time Spent On' => -> (timelog) { timelog.spent_at || timelog.created_at },
      'Time Spent By' => -> (timelog) { timelog.user.username }
    }

    def initialize(csv, issue, until_condition, headers)
      @csv = csv
      @issue = issue
      @until_condition = until_condition
      @headers = headers
    end

    def execute
      create_row_pattern

      @issue.timelogs.each do |timelog|
        csv << insert_values_in_pattern(timelog)

        if until_condition.call
          break
        end
      end
    end

    private

    def create_row_pattern
      @pattern = Array.new(headers.size)

      headers.each.with_index do |element, index|
        @pattern[index] = element if TIMELOG_FIELDS.keys.include?(element)
      end
    end

    def insert_values_in_pattern(timelog)
      row_pattern = @pattern.dup

      row_pattern.map do |column|
        column = TIMELOG_FIELDS[column].call(timelog) if TIMELOG_FIELDS.keys.include?(column)
      end
    end
  end
end
